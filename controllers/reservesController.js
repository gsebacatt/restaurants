import mongoose from "mongoose";
import ReservationsModel from '../models/reservationsModel.js'



const getListReserves = async (req, res, next) => {
  try {
    const list = await ReservationsModel.find()
    .populate('restaurant').populate('table').populate('client');
    res.status(200).json({data: list, success: true});
  } catch (error) {
    next(error);
  }
};

const filterReserves = async (req, res, next) => {
  try {
    const { restaurant, date, client } = req.query;
    
    const list = await ReservationsModel.find({restaurant, client, date})
    res.status(200).json({data: list, success: true});
  } catch (error) {
    next(error);
  }
};



const newReserve = async (req, res, next) => {
  try {
    const { restaurant, table, date, upTo, client,amount } = req.body;

    const newReservation = new ReservationsModel({ restaurant, table, date, upTo, client,amount} );

    const newReservationCreated = await newReservation.save();

    return res.status(200).json({ data: newReservationCreated, success: true });
  } catch (error) {
    next(error);
  }
};




export default {
  getListReserves,
  filterReserves,
  newReserve,
}