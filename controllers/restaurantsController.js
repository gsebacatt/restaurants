import mongoose from "mongoose";
import RestaurantModel from '../models/restaurantModel.js'
import TablesModel from '../models/tablesModel.js'


const getListRestaurants = async (req, res, next) => {
  try {
    const list = await RestaurantModel.find();
    res.status(200).json({data: list, success: true});
  } catch (error) {
    next(error);
  }
};


const getTables = async (req, res, next) => {
  try {
    const {restaurant} = req.query;
    
    const list = await TablesModel.find({restaurant});
    
    res.status(200).json({data: list, success: true});
  } catch (error) {
    next(error);
  }
};



const newRestaurant = async (req, res, next) => {
  try {
    const { name,address } = req.body;

    const newRestaurant = new RestaurantModel({ name, address} );

    const newRestaurantCreated = await newRestaurant.save();

    return res.status(200).json({ data: newRestaurantCreated, success: true });
  } catch (error) {
    next(error);
  }
};




export default {
  getListRestaurants,
  getTables,
  newRestaurant,
}