export class ErrorHandler extends Error {
  constructor(statusCode, message) {
    super();
    this.statusCode = statusCode;
    this.message = message;
  }
}

export const handleError = (err, res) => {
  const { statusCode, message } = err;
  console.log("Error: ", err);

  if (!statusCode) {
    res.status(500).json({
      status: "error",
      statusCode: 500,
      message: "Internal Server Error",
      success: false,
    });

    console.error("500 - Internal Server Error: ");
    console.error(JSON.stringify(err, null, 2));
    return;
  }

  res.status(statusCode).json({
    status: "error",
    statusCode,
    message,
    success: false,
  });
};

export default ErrorHandler;