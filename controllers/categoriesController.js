import CategoryModel from '../models/categoryModel.js'

const getCategories = async (req, res, next) => {
  try {
    const list = await CategoryModel.find();
    res.status(200).json({data: list, success: true});
  } catch (error) {
    next(error);
  }
};

const newCategory = async (req, res, next) => {
  try {
    const { name} = req.body;

    const newCategory = new CategoryModel({ name } );

    const newCategoryCreated = await newCategory.save();

    return res.status(200).json({ data: newCategoryCreated, success: true });
  } catch (error) {
    next(error);
  }
};

const deleteCategory = async (req, res, next) => {
  try {
    const { id } = req.params;

    const categoryRemoved = await CategoryModel.deleteOne({_id: id})

    return res.status(200).json({ data: categoryRemoved, success: true });
  } catch (error) {
    next(error);
  }
};




export default {
  getCategories,
  newCategory,
  deleteCategory,
}