import mongoose from 'mongoose';

const {Schema} = mongoose;

const categorySchema = new Schema({
  id: Number,
  name: String,
})


const CategoryModel = mongoose.model("category", categorySchema);

export default CategoryModel;