import mongoose from "mongoose";
import ClientModel from '../models/clientModel.js'


const getListClients = async (req, res, next) => {
  try {
    const list = await ClientModel.find();
    res.status(200).json({data: list, success: true});
  } catch (error) {
    next(error);
  }
};

const newClient = async (req, res, next) => {
  try {
    const { name,lastName, passport } = req.body;

    const newClient = new ClientModel({ name, lastName, passport} );

    const newClientCreated = await newClient.save();

    return res.status(200).json({ data: newClientCreated, success: true });
  } catch (error) {
    next(error);
  }
};




export default {
  getListClients,
  newClient,
}