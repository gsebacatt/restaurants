import { Router } from 'express'
import clientsController from '../controllers/clientsController.js'

const clientsRouter = Router();

clientsRouter.get('/list', clientsController.getListClients)
clientsRouter.post('/new', clientsController.newClient)


export default clientsRouter;