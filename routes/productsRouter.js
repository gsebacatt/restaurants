import Router from 'express'

import productsController from '../controllers/productsController.js'

const productsRouter = Router();

productsRouter.get('/list', productsController.getProducts)
productsRouter.post('/new', productsController.newProduct)
productsRouter.delete('/:id', productsController.deleteProduct)

export default productsRouter;