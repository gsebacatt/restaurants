import express from "express";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import cors from "cors";
import { handleError } from './errors.js'
import restaurantRoutes from './routes/restaurantRoutes.js';
import reservesRoutes from './routes/reservesRoutes.js';
import clientsRouter from './routes/clientsRoutes.js';
import tablesRoutes from './routes/tablesRoutes.js';

import dotenv from "dotenv";
import categoriesRouter from './routes/categoriesRoutes.js'
import productsRouter from './routes/productsRouter.js'
import servicesRouter from './routes/servicesRouter.js'

dotenv.config();

const app = express();

const port = process.env.PORT || 3001;

let corsOptions = {
  origin: "http://localhost:3001"

};

mongoose.connect(process.env.DB_URI, );
const db = mongoose.connection;

db.on("error", error => console.error(error));
db.once("open", () => console.log("Mongoose Connected to database"));

app.use(cors(corsOptions));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/api/restaurants", restaurantRoutes);
app.use("/api/tables", tablesRoutes);
app.use("/api/clients", clientsRouter);
app.use("/api/reserves",reservesRoutes);
app.use("/api/categories", categoriesRouter);
app.use("/api/products", productsRouter);
app.use("/api/services", servicesRouter);

app.use((err, _, res, __) => {
  handleError(err, res);
});

app.listen(port, () => {
  console.log("Server is up!");
});