import { Router } from 'express'
import reservesController from '../controllers/reservesController.js'

const reserveRouter = Router();

reserveRouter.get('/list', reservesController.getListReserves)
reserveRouter.get('/filter', reservesController.filterReserves)
reserveRouter.post('/new', reservesController.newReserve)


export default reserveRouter;