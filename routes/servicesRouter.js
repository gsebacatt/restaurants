import Router from 'express';

import servicesController from '../controllers/servicesController.js'

const servicesRouter = Router();

servicesRouter.get('/list', servicesController.getServices)
servicesRouter.get('/list/open', servicesController.getOpenServices)
servicesRouter.get('/:id', servicesController.getService)
servicesRouter.post('/start', servicesController.startService)
servicesRouter.post('/end/:id', servicesController.endService)
servicesRouter.post('/detail/:id', servicesController.addDetailToService)
servicesRouter.get('/total/:id', servicesController.getServiceTotal)

export default servicesRouter;