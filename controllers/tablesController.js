import mongoose from "mongoose";
import TablesModel from '../models/tablesModel.js'


const getListTables = async (req, res, next) => {
  try {
    const list = await TablesModel.find().populate('restaurant');
    res.status(200).json({data: list, success: true});
  } catch (error) {
    next(error);
  }
};

const newTable = async (req, res, next) => {
  try {
    const { name,restaurant } = req.body;

    const newTable = new TablesModel({ name, restaurant} );

    const newTableCreated = await newTable.save();

    return res.status(200).json({ data: newTableCreated, success: true });
  } catch (error) {
    next(error);
  }
};


export default {
  getListTables,
  newTable,
}