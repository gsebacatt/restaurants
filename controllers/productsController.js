import ProductModel from '../models/productModel.js'
import CategoryModel from '../models/categoryModel.js'

const getProducts = async (req, res, next) => {
  try {
    const list = await ProductModel.find().populate("category");
    res.status(200).json({data: list, success: true});
  } catch (error) {
    next(error);
  }
};

const newProduct = async (req, res, next) => {
  try {
    const { name, price, category } = req.body;

    const newProduct = new ProductModel({ name, price, category } );

    const newProductCreated = await newProduct.save();

    return res.status(200).json({ data: newProductCreated, success: true });
  } catch (error) {
    console.log(error)
    next(error);
  }
};

const deleteProduct = async (req, res, next) => {
  try {
    const { id } = req.params;

    const productRemoved = await ProductModel.deleteOne({_id: id})

    return res.status(200).json({ data: productRemoved, success: true });
  } catch (error) {
    next(error);
  }
};




export default {
  getProducts,
  newProduct,
  deleteProduct,
}