import mongoose from 'mongoose';

const {Schema} = mongoose;

const productSchema = new Schema({
  id: Number,
  name: String,
  category: {type: mongoose.Types.ObjectId, ref: "category"},
  price: Number,
})


const ProductModel = mongoose.model("products", productSchema);

export default ProductModel;