import ServiceModel from '../models/serviceModel.js'
import mongoose from 'mongoose'
import ProductModel from '../models/productModel.js'

const getServices = async (req,res,next) => {
  try {
    const list = await ServiceModel.find().populate([
      {path: "client"},
      {path: "details",
        populate: {path: "product"}}
    ])
    res.status(200).json({data: list, success: true});
  } catch (error) {
    next(error);
  }
}

const getOpenServices = async (req,res,next) => {
  try {
    const list = await ServiceModel.find({status: 'open'}).populate([
      {path: "client"},
      {path: "details",
        populate: {path: "product"}}
    ])
    res.status(200).json({data: list, success: true});
  } catch (error) {
    next(error);
  }
}

const getService = async (req,res,next) => {
  try {
    const {id} = req.params;
    
    const service = await ServiceModel.findById(id).populate([
      {path: "client"},
      {path: "details",
      populate: {path: "product"}}
    ])
    res.status(200).json({data: service, success: true});
  } catch (error) {
    next(error);
  }
}

const startService = async (req,res,next) => {
  try {
    const { reserve, table, client } = req.body;
    
    let existingServiceForReserve = await ServiceModel.findOne({reserve: reserve})
    
    if(existingServiceForReserve){
      return res.status(400).json({message: "Service already started for reserve"})
    }

    const newService = new ServiceModel({ reserve,table, client, status: 'open', dateOpen: new Date()})

    const newServiceStarted = await newService.save();
    
    return res.status(200).json({data: newServiceStarted, success: true})
  } catch (error) {
    console.log(error)
    next(error)
  }
}

const endService = async (req,res,next) => {
  try {
    const { id } = req.params;
    
    const closedService = await ServiceModel.findByIdAndUpdate(id, {status: "closed", dateClosed: new Date()}, {new : true})

    return res.status(200).json({data:  closedService, success: true})
  } catch (error) {
    console.log(error)
    next(error)
  }
}

const addDetailToService = async (req,res,next) => {
  try {
    const { id } = req.params;
    const {product, amount} = req.body
    
    const detail = {product, amount};

    const updatedService = await ServiceModel.findByIdAndUpdate(id, {$push : {details: detail}}, {new : true})

    return res.status(200).json({data:  updatedService, success: true})
  } catch (error) {
    console.log(error)
    next(error)
  }
}

const getServiceTotal = async (req,res,next) => {
  try {

    const { id } = req.params;

    const service = await ServiceModel.findById(id);

    let total = 0;

    if(service){
      for (let detail of service.details) {
          let product  = await ProductModel.findById(detail.product)
        
          let productTotal = product.price * detail.amount;
          
          total+=productTotal;
      }
    }

    return res.status(200).json({total, success: true})
  }catch(error){
    console.log(error)
    next(error)
  }
}

export default {
  getServices,
  getOpenServices,
  getService,
  startService,
  endService,
  addDetailToService,
  getServiceTotal,
}