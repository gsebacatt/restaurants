import mongoose from "mongoose";

const { Schema } = mongoose;

const tablesSchema = new Schema({
  id: Number,
  name: String,
  restaurant: { type: mongoose.Schema.Types.ObjectId, ref: "restaurants" },
});

const TablesModel = mongoose.model("tables", tablesSchema);

export default TablesModel;