import Router from 'express'

import categoriesController from '../controllers/categoriesController.js'

const categoriesRouter = Router();

categoriesRouter.get('/list', categoriesController.getCategories)
categoriesRouter.post('/new', categoriesController.newCategory)
categoriesRouter.delete('/:id', categoriesController.deleteCategory)


export default categoriesRouter;