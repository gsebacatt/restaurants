import mongoose from "mongoose";

const { Schema } = mongoose;

const reservationsSchema = new Schema({
  id: Number,
  restaurant: { type: mongoose.Schema.Types.ObjectId, ref: "restaurants" },
  table: { type: mongoose.Schema.Types.ObjectId, ref: "tables" },
  date: Date,
  upTo: Date, 
  client: { type: mongoose.Schema.Types.ObjectId, ref: "clients"},
  amount: Number,
});

const ReservationsModel = mongoose.model("reservations", reservationsSchema);

export default ReservationsModel;