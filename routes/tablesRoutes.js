import { Router } from 'express'
import tablesController from '../controllers/tablesController.js'

const tablesRouter = Router();

tablesRouter.get('/list', tablesController.getListTables)
tablesRouter.post('/new', tablesController.newTable)


export default tablesRouter;