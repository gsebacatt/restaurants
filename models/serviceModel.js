import mongoose from 'mongoose'

const { Schema } = mongoose;

const serviceSchema = new Schema({
  id: Number,
  reserve: {type: mongoose.Types.ObjectId, ref: 'reservations'},
  table: {type: mongoose.Types.ObjectId, ref: "tables"},
  client: {type: mongoose.Types.ObjectId, ref: "clients"},
  status: String,
  total: Number,
  dateOpen: Date,
  dateClosed: Date,
  details: [{product: {type: mongoose.Types.ObjectId, ref: "products"}, amount: Number}]
});

const ServiceModel = mongoose.model("services", serviceSchema);

export default ServiceModel;