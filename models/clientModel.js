import mongoose from "mongoose";

const { Schema } = mongoose;

const clientsSchema = new Schema({
  id: Number,
  name: String,
  lastName: String,
  passport: {type: String, unique: true},
});

const ClientsModel = mongoose.model("clients", clientsSchema);

export default ClientsModel;