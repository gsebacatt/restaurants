import mongoose from "mongoose";

const { Schema } = mongoose;

const restaurantSchema = new Schema({
  id: Number,
  name: String,
  address: String,
});

const RestaurantModel = mongoose.model("restaurants", restaurantSchema);

export default RestaurantModel;