import { Router } from 'express'
import restaurantsController from '../controllers/restaurantsController.js'

const restaurantRouter = Router();

restaurantRouter.get('/list', restaurantsController.getListRestaurants)
restaurantRouter.get('/tables', restaurantsController.getTables)
restaurantRouter.post('/new', restaurantsController.newRestaurant)


export default restaurantRouter;